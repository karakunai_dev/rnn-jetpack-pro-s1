**For convenience, please read this file from a markdown reader.**

# Testing Procedures **[ID-id]**

    Sebelum membahas skenario unit dan instrument testing yang akan dilakukan, Saya akan memberikan gambaran tentang konsep dari submission yang saya buat terlebih dahulu. Berikut ini konsep dari RNN Jetpack Pro S1 :

## Konsep 

Submission ini menerapkan konsep `SectionsViewPager` yang diimplementasikan pada `/adapter/ShowsSectionAdapter` yang akan memanggil fragmen `/fragments/ShowsFragment` untuk menampilkan list objek `Show` yang dapat berupa `Movies` ataupun `TV Shows`.  

Perbedaan diantara `ShowsFragment`untuk `Movies` dan `TV Shows` berada pada posisi index yang dipassing kedalam `ShowsFragment` ketika method `getItem(position: Int)` pada `ShowsSectionAdapter` terpanggil yang kemudian digunakan oleh `ShowsFragment` sebagai nilai `ContentDescription` pada RecylerView `RVShows` untuk memudahkan proses pencarian RecyclerView karena tanpanya, akan muncul dua RecyclerView dengan ID yang sama membuat seleksi tambahan berdasarkan `ContentDescription` menjadi alternatif yang saya pilih ketimbang menciptakan dua layout ataupun fragmen yang identik. Untuk `Movies` berada pada indeks 0, dan untuk `TV Shows` berada pada indeks 1.  

Aturan main serupa juga terdapat pada `/viewmodels/ShowViewModel`. ViewModel ini memiliki dua method yakni `getData()` dan `getShows(int: Int)`.
- `getData()` berfungsi untuk mengambil data yang telah diinisialisasikan pada `application/Core.rawShowsData` ketika aplikasi dijalankan untuk menghindari adanya insialisasi yang berulang-ulang.
- `getShows(int: Int)` berfungsi untuk memberikan data dari `getData()` yang telah difilter berdasarkan properti `showType` menurut parameter `int` yang diterima method `getShows()` yang juga menerapkan aturan main indeks 0 untuk `Movies` dan indeks 1 untuk `TV Shows`.  

Sebagai tambahan, `/application/Core` memiliki properti `rawShowsData` yang memanggil method `init()` dari objek `/objects/RawShowsData`.

## Unit Testing

Silakan merujuk pada file `/test/../ShowViewModelTest.kt` mengenai rincian nilai yang di jadikan acuan perbandingan. Terdapat pula komentar mengenai proses testing pada setiap baris pengujian pada file tersebut.

* ShowViewModelTest

    * getData() [`Movies` dan `TV Shows`]

        * Null Check 
        > Apakah ada data yang dikembalikan ?

        * Size Check 
        > Apakah ukuran dari data yang dikembalikan sesuai ?

        * item.showName Check 
        > Apakan properti showName dari item ke n sesuai ?

        * item.showType Check
        > Apakan properti showType dari item ke n sesuai ?

        * item.releaseYear Check
        > Apakan properti releaseYear dari item ke n sesuai ?

        * item.showOverview Check
        > Apakan properti showOverview dari item ke n sesuai ?

        * item.durationHours Check
        > Apakan properti durationHours dari item ke n sesuai ?

        * item.durationMinutes Check
        > Apakan properti durationMinutes dari item ke n sesuai ?

        * item.showGenre Check
        > Apakan properti showGenre dari item ke n sesuai ?

        * item.contentRating Check
        > Apakan properti contentRating dari item ke n sesuai ?

        * item.sourceLink Check
        > Apakan properti sourceLink dari item ke n sesuai ?

    * getShows(0) [`Movies` Saja]

        * Null Check 
        > Apakah ada data yang dikembalikan ?

        * Size Check 
        > Apakah ukuran dari data yang dikembalikan sesuai ?

        * item.showName Check 
        > Apakan properti showName dari item ke n sesuai ?

        * item.showType Check
        > Apakan properti showType dari item ke n sesuai ?

        * item.releaseYear Check
        > Apakan properti releaseYear dari item ke n sesuai ?

        * item.showOverview Check
        > Apakan properti showOverview dari item ke n sesuai ?

        * item.durationHours Check
        > Apakan properti durationHours dari item ke n sesuai ?

        * item.durationMinutes Check
        > Apakan properti durationMinutes dari item ke n sesuai ?

        * item.showGenre Check
        > Apakan properti showGenre dari item ke n sesuai ?

        * item.contentRating Check
        > Apakan properti contentRating dari item ke n sesuai ?

        * item.sourceLink Check
        > Apakan properti sourceLink dari item ke n sesuai ?

    * getShows(1) [`TV Shows` Saja]

        * Null Check 
        > Apakah ada data yang dikembalikan ?

        * Size Check 
        > Apakah ukuran dari data yang dikembalikan sesuai ?

        * item.showName Check 
        > Apakan properti showName dari item ke n sesuai ?

        * item.showType Check
        > Apakan properti showType dari item ke n sesuai ?

        * item.releaseYear Check
        > Apakan properti releaseYear dari item ke n sesuai ?

        * item.showOverview Check
        > Apakan properti showOverview dari item ke n sesuai ?

        * item.durationHours Check
        > Apakan properti durationHours dari item ke n sesuai ?

        * item.durationMinutes Check
        > Apakan properti durationMinutes dari item ke n sesuai ?

        * item.showGenre Check
        > Apakan properti showGenre dari item ke n sesuai ?

        * item.contentRating Check
        > Apakan properti contentRating dari item ke n sesuai ?

        * item.sourceLink Check
        > Apakan properti sourceLink dari item ke n sesuai ?

## Instrument Testing (Espresso)

Pada dasarnya pengujian pada `MainActivityTest` dan `DetailsActivityTest` sangat identik, perbedaannya yakni pada `DetailsActivityTest` dimana ada pengujian lebih lanjut dengan mengarahkan pengujian kedalam `DetailsActivity` sedangakan pada `MainActivityTest` hanya sebatas pengujian `isDisplayed()` dan `scrollToPosition()` saja untuk menguji keberadaan item pada ViewHolder di masing-masing RecyclerView pada `MainActivity`. Pada kedua pengujian memanggil fungsi `showLoadTest()` untuk melakukan pengujian pada item ke n...sampai...n untuk menghindari baris pengujian yang diulang secara manual.

* MainActivityTest

    * loadMovies() [`Movies` saja] [-> `MainActivity`]

        <!-- MainActivity -->

        * Null Check 
        > Apakah ada data yang dikembalikan ?

        * showsLoadTest()
        > Menguji item pada ViewHolder untuk item Shows bertipe `Movies` pada indeks ke n...sampai..n  

    * loadTVShows() [`TV Shows` saja] [-> `MainActivity`]

        <!-- MainActivity -->

        * Null Check 
        > Apakah ada data yang dikembalikan ?

        * showsLoadTest()
        > Menguji item pada ViewHolder untuk item Shows bertipe `TV Shows` pada indeks ke n...sampai..n  

    * Fungsi showLoadTest() [-> `MainActivity`]

        <!-- MainActivity -->

        * TabView Title Check
        > Apakah ada TabView dengan teks `Movies` / `TV Shows` ? dan bisa `click()` ?

        * Item at n position Check
        > Apakah item pada posisi indeks ke n...sampai...n `isDisplayed()` ? dan bisa `scrollToPosition()` ?

* DetailsActivityTest

    * loadMovies() [`Movies` saja] [-> `MainActivity`]

        <!-- MainActivity -->

        * Null Check
        > Apakah ada data yang dikembalikan ?

        * showsLoadTest()
        > Menguji item pada ViewHolder untuk item Shows bertipe `Movies` pada indeks ke n...sampai..n  

    * loadTVShows() [`TV Shows` saja] [-> `MainActivity`]

        <!-- MainActivity -->

        * Null Check
        > Apakah ada data yang dikembalikan ?

        * showsLoadTest()
        > Menguji item pada ViewHolder untuk item Shows bertipe `TV Shows` pada indeks ke n...sampai..n  

    * Fungsi showLoadTest() [-> `MainActivity` -> `DetailsActivity` -> `Web Browser` -> `MainActivity`]

        <!-- MainActivity -->

        * TabView Title Check
        > Apakah ada TabView dengan teks `Movies` / `TV Shows` 
        ? dan bisa `click()` ?

        * Item at n position Check
        > Apakah item pada posisi indeks ke n...sampai...n `isDisplayed()` ? dan bisa `scrollToPosition()` ? dan bisa `click()` untuk menuju halaman `DetailsActivity` ?

        <!-- DetailsActivity -->

        * R.id.IVPoster Check
        > Apakah view dengan ID R.id.IVPoster ada dan `isDisplayed()` ?

        * R.id.TVTitle Check
        > Apakah view dengan ID R.id.TVTitle ada dan `isDisplayed()` ?

        * R.id.TVYear Check
        > Apakah view dengan ID R.id.TVYear ada dan `isDisplayed()` ?

        * R.id.TVOverview Check
        > Apakah view dengan ID R.id.TVOverview ada dan `isDisplayed()` ?

        * R.id.TVGenre Check
        > Apakah view dengan ID R.id.TVGenre ada dan `isDisplayed()` ?

        * R.id.TVType Check
        > Apakah view dengan ID R.id.TVType ada dan `isDisplayed()` ?

        * R.id.TVContentRating Check
        > Apakah view dengan ID R.id.TVContentRating ada dan `isDisplayed()` ?

        * R.id.TVDuration Check
        > Apakah view dengan ID R.id.TVDuration ada dan `isDisplayed()` ?

        * R.id.BSource Check
        > Apakah view dengan ID R.id.BSource ada dan `isDisplayed()` ? dan bisa `click()` ? yang kemudian akan secara otomatis membuka halaman TMDB dari Show terkait menurut properti `sourceLink` pada sebuah Web Browser

        <!-- Web Browser -->

        * Relaunch MainActivity
        > Apakah MainActivity dapat diluncurkan kembali seperti semula setelah keluar dari aplikasi dan meluncurkan Wen Browser ?

        <!-- MainActivity -->

        