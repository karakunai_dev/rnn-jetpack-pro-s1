package rnn.practice.rnnjetpackpros1.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.junit.rules.TestRule

class ShowViewModelTest {

    private var viewModel: ShowViewModel? = null
    private var checkCounter = 0

    @Before
    fun testConfig() {
        viewModel = ShowViewModel()
    }

    // A rule to allow LiveData setValue() and getValue() tests to work (for future purposes)
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun getData() {

        println("# Currently Asserting -> viewModel.getData()")

        // Null Check before asserting the rest of the lines
        println("> getData() Null Check")
        assertNotNull(viewModel?.getData())
        checkCounter++

        with(viewModel?.getData()!!) {

            // Size Check
            println("> getData() Arraylist Size Check")
            assertEquals(39, this.size)
            checkCounter++

            // Does the element at index 3 have a showName value of "Bohemian Rhapsody" ?
            println("> getData() item.showName Check")
            assertEquals("Bohemian Rhapsody", this[3].showName)
            checkCounter++

            // Does the element at index 19 have a showType value of 1 ?
            println("> getData() item.showType Check")
            assertEquals(1, this[19].showType)
            checkCounter++

            // Does the element at index 36 have a releaseYear value of "1989" ?
            println("> getData() item.releaseYear Check")
            assertEquals("1989", this[36].releaseYear)
            checkCounter++

            // Does the element at index 27 have a showOverview value of "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital." ?
            println("> getData() item.showOverview Check")
            assertEquals("Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.", this[27].showOverview)
            checkCounter++

            // Does the element at index 38 have a durationHour value of 0 ?
            println("> getData() item.durationHours Check")
            assertEquals(0, this[38].durationHours)
            checkCounter++

            // Does the element at index 33 have a durationMinutes value of 0" ?
            println("> getData() item.durationMinutes Check")
            assertEquals(0, this[33].durationMinutes)
            checkCounter++

            // Does the element at index 22 have a showGenre value of "Action & Adventure, Animation, Comedy, Science Fiction, Fantasy, Mystery" ?
            println("> getData() item.showGenre Check")
            assertEquals("Action & Adventure, Animation, Comedy, Science Fiction, Fantasy, Mystery", this[22].showGenre)
            checkCounter++

            // Does the element at index 18 have a contentRating value of "12+" ?
            println("> getData() item.contentRating Check")
            assertEquals("12+", this[18].contentRating)
            checkCounter++

            // Does the element at index 30 have a sourceLink value of "https://www.themoviedb.org/tv/31910-naruto-shipp-Data" ?
            println("> getData() item.sourceLink Check")
            assertEquals("https://www.themoviedb.org/tv/31910-naruto-shipp-den", this[30].sourceLink)
            checkCounter++
        }
    }

    @Test
    fun getShows() {

        println("# Currently Asserting -> viewModel.getShows()")

        // Null Check before asserting the rest of the lines
        println("> getShows() [Movies] Null Check")
        assertNotNull(viewModel?.getShows(0))
        checkCounter++

        with(viewModel?.getShows(0)!!) {
            // [Movies] Size Check
            println("> getShows() [Movies] Arraylist Size Check")
            assertEquals(19, this.size)
            checkCounter++

            // [Movies] Does the element at index 3 have a showName value of "Bohemian Rhapsody" ?
            println("> getShows() [Movies] item.showName Check")
            assertEquals("Bohemian Rhapsody", this[3].showName)
            checkCounter++

            // [Movies] Does the element at index 18 have a showType value of 0 ?
            println("> getShows() [Movies] item.showType Check")
            assertEquals(0, this[18].showType)
            checkCounter++

            // [Movies] Does the element at index 6 have a releaseYear value of "2018" ?
            println("> getShows() [Movies] item.releaseYear Check")
            assertEquals("2018", this[6].releaseYear)
            checkCounter++

            // [Movies] Does the element at index 15 have a showOverview value of "A war-hardened Crusader and his Moorish commander mount an audacious revolt against the corrupt English crown." ?
            println("> getShows() [Movies] item.showOverview Check")
            assertEquals("A war-hardened Crusader and his Moorish commander mount an audacious revolt against the corrupt English crown.", this[15].showOverview)
            checkCounter++

            // [Movies] Does the element at index 0 have a durationHour value of 2 ?
            println("> getShows() [Movies] item.durationHours Check")
            assertEquals(2, this[0].durationHours)
            checkCounter++

            // [Movies] Does the element at index 1 have a durationMinutes value of 2" ?
            println("> getShows() [Movies] item.durationMinutes Check")
            assertEquals(2, this[1].durationMinutes)
            checkCounter++

            // [Movies] Does the element at index 5 have a showGenre value of "Drama" ?
            println("> getShows() [Movies] item.showGenre Check")
            assertEquals("Drama", this[5].showGenre)
            checkCounter++

            // [Movies] Does the element at index 8 have a contentRating value of "SU" ?
            println("> getShows() [Movies] item.contentRating Check")
            assertEquals("SU", this[8].contentRating)
            checkCounter++

            // [Movies] Does the element at index 7 have a sourceLink value of "https://www.themoviedb.org/movie/450465-glass" ?
            println("> getShows() [Movies] item.sourceLink Check")
            assertEquals("https://www.themoviedb.org/movie/450465-glass", this[7].sourceLink)
            checkCounter++
        }

        // Null Check before asserting the rest of the lines
        println("> getShows() [TV Shows] Null Check")
        assertNotNull(viewModel?.getShows(1))
        checkCounter++

        with(viewModel?.getShows(1)!!) {
            // [TV Shows] Size Check
            println("> getShows() [TV Shows] Arraylist Size Check")
            assertEquals(20, this.size)
            checkCounter++

            // [TV Shows] Does the element at index 3 have a showName value of "Fairy Tail" ?
            println("> getShows() [TV Shows] item.showName Check")
            assertEquals("Fairy Tail", this[3].showName)
            checkCounter++

            // [TV Shows] Does the element at index 18 have a showType value of 1 ?
            println("> getShows() [TV Shows] item.showType Check")
            assertEquals(1, this[18].showType)
            checkCounter++

            // [TV Shows] Does the element at index 6 have a releaseYear value of "2011" ?
            println("> getShows() [TV Shows] item.releaseYear Check")
            assertEquals("2011", this[6].releaseYear)
            checkCounter++

            // [TV Shows] Does the element at index 8 have a showOverview value of "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital." ?
            println("> getShows() [TV Shows] item.showOverview Check")
            assertEquals("Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.", this[8].showOverview)
            checkCounter++

            // [TV Shows] Does the element at index 0 have a durationHour value of 0 ?
            println("> getShows() [TV Shows] item.durationHours Check")
            assertEquals(0, this[0].durationHours)
            checkCounter++

            // [TV Shows] Does the element at index 1 have a durationMinutes value of 49" ?
            println("> getShows() [TV Shows] item.durationMinutes Check")
            assertEquals(49, this[1].durationMinutes)
            checkCounter++

            // [TV Shows] Does the element at index 5 have a showGenre value of "Drama, Science Fiction, Fantasy" ?
            println("> getShows() [TV Shows] item.showGenre Check")
            assertEquals("Drama, Science Fiction, Fantasy", this[5].showGenre)
            checkCounter++

            // [TV Shows] Does the element at index 8 have a contentRating value of "TV-14" ?
            println("> getShows() [TV Shows] item.contentRating Check")
            assertEquals("TV-14", this[8].contentRating)
            checkCounter++

            // [TV Shows] Does the element at index 7 have a sourceLink value of "https://www.themoviedb.org/tv/60708-gotham" ?
            println("> getShows() [TV Shows] item.sourceLink Check")
            assertEquals("https://www.themoviedb.org/tv/60708-gotham", this[7].sourceLink)
            checkCounter++
        }
    }

    @After
    fun postTest() {
        viewModel = null
        println("# $checkCounter Checks Passed\n")
    }
}