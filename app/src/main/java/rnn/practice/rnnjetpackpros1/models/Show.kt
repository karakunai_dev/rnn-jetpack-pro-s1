package rnn.practice.rnnjetpackpros1.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Show(
    var showName: String? = null,
    var showOverview: String? = null,
    var showGenre: String? = null,
    var contentRating: String? = null,
    var sourceLink: String? = null,
    var releaseYear: String? = null,
    var imgRes: Int? = 0,
    var showType: Int? = 0,
    var durationHours: Int? = 0,
    var durationMinutes: Int? = 0
) : Parcelable
