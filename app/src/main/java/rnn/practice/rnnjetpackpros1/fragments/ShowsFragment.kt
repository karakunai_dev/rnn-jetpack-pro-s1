package rnn.practice.rnnjetpackpros1.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rnn.practice.rnnjetpackpros1.DetailsActivity
import rnn.practice.rnnjetpackpros1.adapters.ShowAdapter
import rnn.practice.rnnjetpackpros1.databinding.FragmentShowsBinding
import rnn.practice.rnnjetpackpros1.viewmodels.ShowViewModel

class ShowsFragment(private val showType: Int) : Fragment() {

    private lateinit var binding: FragmentShowsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShowsBinding.inflate(layoutInflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(ShowViewModel::class.java)

        if (activity != null) {
            binding.RVShows.apply {
                adapter = ShowAdapter(viewModel.getShows(showType)) {
                    startActivity(
                        Intent(context, DetailsActivity::class.java)
                            .putExtra("currentShowIndex", it)
                            .putExtra("currentShowType", showType)
                    )
                }
                layoutManager = LinearLayoutManager(context)
                contentDescription = showType.toString()
                setHasFixedSize(true)
            }
        }
    }
}