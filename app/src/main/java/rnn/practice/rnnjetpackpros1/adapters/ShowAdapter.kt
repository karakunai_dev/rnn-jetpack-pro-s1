package rnn.practice.rnnjetpackpros1.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import rnn.practice.rnnjetpackpros1.databinding.ItemShowBinding
import rnn.practice.rnnjetpackpros1.models.Show

class ShowAdapter(private val arrayList: ArrayList<Show>, private val listener: (Int) -> Unit) : RecyclerView.Adapter<ShowAdapter.ShowsViewHolder>() {

    inner class ShowsViewHolder(private val binding: ItemShowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun getBinding() = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowsViewHolder {
        return ShowsViewHolder(ItemShowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ShowsViewHolder, position: Int) {
        holder.apply {
            itemView.setOnClickListener { listener(position) }

            getBinding().apply {

                arrayList[position].also { current ->
                    Glide.with(itemView)
                        .load(current.imgRes)
                        .into(IVPoster)

                    TVTitle.text = current.showName
                    TVYear.text = current.releaseYear
                    TVOverview.text = current.showOverview
                }
            }
        }
    }

    override fun getItemCount(): Int = arrayList.size
}