package rnn.practice.rnnjetpackpros1.adapters

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import rnn.practice.rnnjetpackpros1.R
import rnn.practice.rnnjetpackpros1.fragments.ShowsFragment

class ShowsSectionAdapter(private val context: Context, fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    companion object {
        @StringRes
        private val TAB_SHOWS_TITLES = intArrayOf(
            R.string.String_Movies,
            R.string.String_TVShows
        )
    }

    override fun getCount(): Int = 2

    override fun getItem(position: Int): Fragment = ShowsFragment(position)

    override fun getPageTitle(position: Int): CharSequence = context.getString(TAB_SHOWS_TITLES[position])
}