package rnn.practice.rnnjetpackpros1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import rnn.practice.rnnjetpackpros1.adapters.ShowsSectionAdapter
import rnn.practice.rnnjetpackpros1.databinding.ActivityMainBinding

private lateinit var binding: ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            VPShows.adapter = ShowsSectionAdapter(this@MainActivity, supportFragmentManager)
            TLShows.setupWithViewPager(VPShows)
        }

        supportActionBar?.elevation = 0f
        supportActionBar?.title = "RNN Shows DB"
    }
}