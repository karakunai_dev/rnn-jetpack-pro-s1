package rnn.practice.rnnjetpackpros1.application

import android.app.Application
import rnn.practice.rnnjetpackpros1.objects.RawShowsData

class Core : Application() {
    val rawShowsData = RawShowsData.init()
}