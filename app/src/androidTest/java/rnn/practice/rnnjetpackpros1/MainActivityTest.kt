package rnn.practice.rnnjetpackpros1

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import junit.framework.TestCase.assertNotNull
import org.hamcrest.CoreMatchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import rnn.practice.rnnjetpackpros1.viewmodels.ShowViewModel

class MainActivityTest {

    private var viewModel: ShowViewModel? = null

    @Before
    fun testConfig() {
        viewModel = ShowViewModel()
    }

    @get:Rule
    var activityRule = activityScenarioRule<MainActivity>()

    @Test
    fun loadMovies() {
        // Check if there are any return for int = 0 (Movies)
        assertNotNull(viewModel?.getShows(0))

        with(viewModel?.getShows(0)!!) {
            // Execute showsLoadTest() for Movies at item position of =
            // this.size - 1 ,
            // 3 ,
            // this.size / 2 + (this.size / 4) ,
            // and this.size / 3
            showsLoadTest("Movies", "0", this.size - 1, 3, (this.size / 2 + (this.size / 4)), (this.size / 3))
        }
    }

    @Test
    fun loadTVShows() {
        // Check if there are any return for int = 0 (TV Shows)
        assertNotNull(viewModel?.getShows(1))

        with(viewModel?.getShows(1)!!) {
            // Execute showsLoadTest() for TV Shows at item position of =
            // this.size - 1 ,
            // 3 ,
            // this.size / 2 + (this.size / 4) ,
            // and this.size / 3
            showsLoadTest("TV Shows", "1", this.size - 1, 3, (this.size / 2  + (this.size / 4)), (this.size / 3))
        }
    }

    @After
    fun postTest() {
        viewModel = null
    }

    private fun showsLoadTest(tabViewString: String, contentDescription: String, vararg positionIndexes: Int) {

        // Testing every item on the ViewHolder based on the amount of item passed into vararg positionIndexes
        for (currentIndex in positionIndexes) {

            // Check if any view with string of tabViewString exists ? is it isDisplayed() ? then perform click()
            onView(withText(tabViewString)).check(matches(isDisplayed())).perform(click())

            // Check if any view with the ID of R.id.RVShows and had a ContentDescription of contentDescription exists ? is it isDisplayed() ? then scroll to currentIndex position
            onView(allOf(withId(R.id.RVShows), withContentDescription(contentDescription))).check(matches(isDisplayed())).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(currentIndex))
        }
    }
}